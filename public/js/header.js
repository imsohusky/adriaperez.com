const updateHeader = () => {
    setHeaderScrolledItemsVisibility(getRelativeSocialIconsPosition() < 0);
}

const getSocialIconsPosition = () => {
    return document.querySelector('.title-container > ul').getBoundingClientRect().bottom;
}

const getRelativeSocialIconsPosition = () => {
    return getSocialIconsPosition() / getViewportHeigh() * 100;
}

const setHeaderScrolledItemsVisibility = (shouldDisplay) => {
    document.querySelectorAll('.header-scrolled').forEach(element => {
        element.style.opacity = shouldDisplay ? 1 : 0;
    });
}

window.addEventListener('scroll', updateHeader);
window.addEventListener('rezise', updateHeader);
